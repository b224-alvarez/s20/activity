// NUMBERS

let inputNum = Number(prompt("Input a number: "));
console.log("The number you provided is " + inputNum + ".");

for (inputNum; inputNum >= 0; inputNum--) {
  if (inputNum <= 50) {
    console.log(
      "The current value is at " + inputNum + ". Terminating the loop."
    );
    break;
  }

  if (inputNum % 10 === 0) {
    console.log("The number is divisible by 10. Skipping the number.");
    continue;
  }

  if (inputNum % 5 === 0) {
    console.log(inputNum);
    continue;
  }
}

console.log("");

// CONSONANTS
let aString = "supercalifragilisticexpialidocious";
let aStringCons = "";

for (let i = 0; i < aString.length; i++) {
  if (
    aString[i].toLowerCase() == "a" ||
    aString[i].toLowerCase() == "e" ||
    aString[i].toLowerCase() == "i" ||
    aString[i].toLowerCase() == "o" ||
    aString[i].toLowerCase() == "u"
  ) {
    continue;
  } else {
    aStringCons += aString[i];
  }
}

console.log(aString);
console.log(aStringCons);
